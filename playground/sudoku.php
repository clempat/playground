<?php

/**
 * Class Sudoku.php
 *
 * @author Clement Patout <clement.patout@gmail.com>
 * Date: 15/05/14
 * Time: 20:09
 */
class Sudoku
{
    /** @var array Sudoku input */
    protected $sudoku = array();
    /** @var array List of square */
    protected $square = array();
    /** @var array transposed matrix */
    protected $transposedSudoku = array();
    /** @var bool true if is valid */
    protected $valid = true;

    /**
     * Constructor
     *
     * @param $input array
     */
    function __construct($input) {
        $this->sudoku = $input;
    }

    /**
     * Validate the Sudoku
     *
     * @return bool true if valid
     */
    public function validate()
    {
        // Check if empty
        if (empty($this->sudoku)) {
            return false;
        }

        // Check Lines
        call_user_func_array(array('Sudoku','validateOneToNineInArray'), $this->sudoku);

        if (!$this->valid) {
            return false;
        }

        // Check Columns
        $this->transposedSudoku = $this->transpose($this->sudoku);
        call_user_func_array(array('Sudoku','validateOneToNineInArray'), $this->transposedSudoku);

        if (!$this->valid) {
            return false;
        }

        // Check Square
        $this->prepareSquare();
        call_user_func_array(array('Sudoku','validateOneToNineInArray'), $this->square);

        return $this->valid;

    }

    /**
     * Prepare the list of square
     */
    protected function prepareSquare()
    {
        $j = 0;
        foreach ($this->sudoku as $idx => $line) {
            // Square
            $keysSquare = array('square' . $j, 'square'. ($j+1), 'square' . ($j+2));

            $this->square = array_merge_recursive(
                $this->square,
                array_combine($keysSquare, array_chunk($line, 3))
            );

            if ($idx != 1 && ($idx+1)%3 == 0) {
                $j = $j + 3;
            }
        }


    }

    /**
     * Transpose a multidimensional array
     * @param $array
     * @return mixed
     */
    function transpose($array) {
        array_unshift($array, null);
        return call_user_func_array('array_map', $array);
    }

    /**
     * Validate that all array has 1 to 9
     *
     * @param $array1
     * @param $array2
     * @param $array3
     * @param $array4
     * @param $array5
     * @param $array6
     * @param $array7
     * @param $array8
     * @param $array9
     */
    function validateOneToNineInArray ($array1, $array2, $array3, $array4, $array5, $array6, $array7, $array8, $array9)
    {
        // Check if one number missing
        $check = range(1,9);
        $validation = array_intersect($check, $array1, $array2, $array3, $array4, $array5, $array6, $array7, $array8, $array9);

        if (count($validation) < 9) {
            $this->valid = false;
        }
    }


}

$sudoku = new Sudoku(
    array(
        range(1,9),
        range(1,9),
        range(1,9),
        range(1,9),
        range(1,9),
        range(1,9),
        range(1,9),
        range(1,9),
        range(1,9),
    )
);

var_dump($sudoku->validate());

$sudokuvalid = new Sudoku(
    array(
        array(4, 6, 9, 2, 5, 8, 7, 1, 3),
        array(7, 1, 8, 3, 9, 4, 6, 2, 5),
        array(5, 3, 2, 7, 6, 1, 8, 4, 9),
        array(1, 9, 5, 6, 8, 3, 2, 7, 4),
        array(3, 8, 7, 5, 4, 2, 1, 9, 6),
        array(6, 2, 4, 9, 1, 7, 5, 3, 8),
        array(2, 4, 6, 1, 3, 5, 9, 8, 7),
        array(8, 5, 1, 4, 7, 9, 3, 6, 2),
        array(9, 7, 3, 8, 2, 6, 4, 5, 1)
    )
);

var_dump($sudokuvalid->validate());

